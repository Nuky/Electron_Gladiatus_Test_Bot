# Expedition data

## Grimwood

### Rat

    - Attack URL:   https://s37-de.gladiatus.gameforge.com/game/ajax.php?mod=location&submod=attack&location=0&stage=1&premium=0&a=1626787899049&sh=8134a0c4213674396ce2dbf8078137b4
    - Report URL:   https://s37-de.gladiatus.gameforge.com/game/index.php?mod=reports&submod=showCombatReport&t=0&reportId=248683&sh=8134a0c4213674396ce2dbf8078137b4

### Lynx

    - Attack URL:   https://s27-us.gladiatus.gameforge.com/game/ajax.php?mod=location&submod=attack&location=0&stage=2&premium=0&a=1626718617749&sh=36bb9e8699c1bf40c95793fcd15b1f14

### Wolf

    - Attack URL:   https://s27-us.gladiatus.gameforge.com/game/ajax.php?mod=location&submod=attack&location=0&stage=3&premium=0&a=1626718617749&sh=36bb9e8699c1bf40c95793fcd15b1f14

### Bear

    - Attack URL:   https://s27-us.gladiatus.gameforge.com/game/ajax.php?mod=location&submod=attack&location=0&stage=4&premium=0&a=1626718617749&sh=36bb9e8699c1bf40c95793fcd15b1f14
