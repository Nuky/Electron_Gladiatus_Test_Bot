export const LOGIN_URL = 'https://gameforge.com/api/v1/auth/thin/sessions'
export const AUTH_URL = 'https://lobby.gladiatus.gameforge.com/api/users/me'
export const USER_ACCOUNT_URL =
    'https://lobby.gladiatus.gameforge.com/api/users/me/accounts'
