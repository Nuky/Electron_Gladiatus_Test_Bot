//Snackbar actions
export const SET_SNACKBAR = 'SET_SNACKBAR'

//App state actions
export const SET_USER_STATE = 'SET_USER_STATE'

//User actions
export const LOGIN_USER = 'LOGIN_USER'
export const SET_ACCOUNTS = 'SET_ACCOUNTS'
export const SET_CURRENT_ACCOUNT = 'SET_CURRENT_ACCOUNT'
export const SET_PLAYER_STATS = ' SET_PLAYER_STATS'

//ENV Actions
export const SET_ENV_LOGIN = 'SET_ENV_LOGIN'
export const SET_ENV_AUTH_DATA = 'SET_ENV_AUTH_DATA'
export const SET_SH_TOKEN = 'SET_SH_TOKEN'

//botLogic
export const SET_EXPEDITION_DATA = 'SET_EXPEDITION_DATA'
