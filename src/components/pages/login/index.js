import React, { Component, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Redirect } from 'wouter'
import { useSelector } from 'react-redux'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

import { loginUser } from './../../../redux/actions/user'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexGrow: 1
    }
}))

const Login = () => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const userLoggedIn = useSelector(state => state.user.userLoggedIn)
    const environmentData = useSelector(state => state.environment)

    const [email, setEmail] = useState('nanox78881@nhmty.com')
    const [password, setPassword] = useState('Passwort123')

    const handleSubmit = e => {
        e.preventDefault()
        dispatch(loginUser({ email, password, environmentData }))
    }
    if (userLoggedIn) {
        return <Redirect to="/"></Redirect>
    }
    return (
        <React.Fragment>
            <form className={classes.form} onSubmit={handleSubmit} noValidate>
                <Grid
                    container
                    className={classes.root}
                    spacing={1}
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    style={{ minHeight: '100vh' }}
                >
                    <Grid item xs={12}>
                        <Typography varian="h1" align="center">
                            Login
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            onChange={event => setEmail(event.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={event => setPassword(event.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                        >
                            Login
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </React.Fragment>
    )
}

export default Login
