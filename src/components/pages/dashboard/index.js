import React from 'react'
import { useSelector } from 'react-redux'
import AccountSelector from './components/accountSelector'
import { useLocation } from 'wouter'

import ExpeditionPage from './../expedition/index'

import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        marginTop: 10
    },
    expedition: {
        direction: 'column'
    },
    expeditionBody: {
        marginTop: 10
    },
    button: {
        width: '90%'
    }
}))

const Dashboard = () => {
    const classes = useStyles()

    const currentAccount = useSelector(state => state.user.currentAccount)
    const charStats = useSelector(state => state.user.stats)
    const [location, setLocation] = useLocation()

    if (currentAccount === null) {
        return <AccountSelector />
    }

    return (
        <React.Fragment>
            <Grid container spacing={3} className={classes.root}>
                <Grid item lg={4} md={6} xl={3} xs={12}>
                    <Paper>
                        <Grid
                            container
                            className={classes.expedition}
                            spacing={2}
                        >
                            <Grid item xs={12}>
                                <Typography variant="h6" align={'center'}>
                                    Expedition
                                </Typography>
                            </Grid>
                            <Grid container className={classes.expeditionBody}>
                                <Grid item xs={6}>
                                    <Typography
                                        variant="body1"
                                        align={'center'}
                                    >
                                        Points:{charStats.expeditionPoints}/
                                        {charStats.expeditionPointsMax}
                                    </Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography
                                        variant="body1"
                                        align={'center'}
                                    >
                                        Status: Running
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} align={'center'}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className={classes.button}
                                    onClick={() => {
                                        setLocation('/expeditionPage')
                                    }}
                                >
                                    <Typography variant="button">
                                        Expedition
                                    </Typography>
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item lg={4} md={6} xl={3} xs={12}>
                    <Paper>Dungeons</Paper>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}

export default Dashboard
//                    <Grid item lg={4} md={6} xl={3} xs={12}></Grid>
