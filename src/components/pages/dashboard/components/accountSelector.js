import React from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import { setCurrentAccount } from '../../../../redux/actions/user'

const useStyles = makeStyles({
    root: {
        display: 'flex',
        flex: '1',
        paddingTop: 10,
        width: '100%',
        height: '100%'
    },
    paper: {
        width: '100%',
        margin: 10,
        padding: 10
    }
})

export default function AccountSelector() {
    const classes = useStyles()
    const dispatch = useDispatch()
    const accounts = useSelector(state => state.user.accounts)

    console.log(accounts)
    let selectAccount = index => {
        dispatch(setCurrentAccount(accounts[index]))
    }
    return (
        <React.Fragment>
            <Grid container className={classes.root}>
                <Grid item xs={12}>
                    <Typography align={'center'}>Select Account</Typography>
                </Grid>
                {accounts.map((account, i) => (
                    <Paper key={i} className={classes.paper}>
                        <Grid container>
                            <Grid item xs={6}>
                                {account.name +
                                    ' ' +
                                    account.server.language +
                                    account.server.number}
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={() => {
                                        selectAccount(i)
                                    }}
                                >
                                    Select Account
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                ))}
            </Grid>
        </React.Fragment>
    )
}
