import React, { useEffect } from 'react'
import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useDispatch, useSelector } from 'react-redux'

import { setExpeditionData } from './../../../redux/actions/botLogic'
import { attackExpedition } from './../../../utils/botLogics/expeditionBot/index'

import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import InputLabel from '@material-ui/core/InputLabel'
import SettingsIcon from '@material-ui/icons/Settings'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexGrow: 1
    },
    paper: {
        width: '100%',
        margin: 10,
        padding: 10
    }
})

export default function ExpeditionPage() {
    const classes = useStyles()
    const dispatch = useDispatch()

    const locations = useSelector(state => state.gameData.locations)
    const expeditionBotLogic = useSelector(
        state => state.botLogic.expeditionBot
    )

    let intervalId = expeditionBotLogic.intervalId

    const [selectedLocation, setSelectedLocation] = useState(
        expeditionBotLogic.attackingLocation
    )

    const [selectedMonster, setSelectedMonster] = useState(
        expeditionBotLogic.attackingMonster
    )
    const [running, setRunning] = useState(expeditionBotLogic.running)
    const [delay, setDelay] = useState(expeditionBotLogic.delay)

    const [monsters, setMonsters] = useState(
        locations[selectedLocation].monsters
    )

    const handleChangeLocation = event => {
        setSelectedLocation(event.target.value)
        updateBotLogic()
    }

    const handleChangeMonster = event => {
        setSelectedMonster(event.target.value)
        updateBotLogic()
    }

    const updateBotLogic = () => {
        dispatch(
            setExpeditionData({
                attackingLocation: selectedLocation,
                attackingMonster: selectedMonster,
                running,
                delay,
                intervalId
            })
        )
    }

    const handleBotToggle = event => {
        if (!running) {
            setTimeout(attackExpedition(), delay)
            intervalId = setInterval(attackExpedition, delay)
        } else {
            clearInterval(intervalId)
            intervalId = ''
        }

        setRunning(!running)
        updateBotLogic()
    }

    useEffect(() => {
        setMonsters(locations[selectedLocation].monsters)
    }, [selectedMonster, locations, selectedLocation])

    return (
        <React.Fragment>
            <Paper className={classes.paper}>
                <Grid container className={classes.root}>
                    <Grid container alignItems={'center'}>
                        <Grid item xs={4}>
                            Back Arrow
                        </Grid>
                        <Grid item xs={4}>
                            <Typography align={'center'}>Expedition</Typography>
                        </Grid>
                        <Grid item xs={4} align={'right'}>
                            <IconButton onClick={() => console.log(monsters)}>
                                <SettingsIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={6}>
                            <Grid item xs={12} lg={6}>
                                <InputLabel id="locationSelectorLabel">
                                    Location
                                </InputLabel>
                                <Select
                                    labelId="locationSelectorLabel"
                                    id="locationSelector"
                                    value={selectedLocation}
                                    onChange={handleChangeLocation}
                                >
                                    {locations.map((location, index) => {
                                        return (
                                            <MenuItem value={index} key={index}>
                                                {location.name}
                                            </MenuItem>
                                        )
                                    })}
                                </Select>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <InputLabel id="monsterSelectorLabel">
                                    Monsters
                                </InputLabel>
                                <Select
                                    labelId="monsterSelectorLabel"
                                    id="monsterSelector"
                                    value={selectedMonster}
                                    onChange={handleChangeMonster}
                                >
                                    {monsters.map((monster, index) => {
                                        return (
                                            <MenuItem value={index} key={index}>
                                                {monster}
                                            </MenuItem>
                                        )
                                    })}
                                </Select>
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={handleBotToggle}
                                >
                                    {running === true ? 'Stop' : 'Start'}
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} style={{ backgroundColor: 'blue' }}>
                            Rechts
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </React.Fragment>
    )
}
