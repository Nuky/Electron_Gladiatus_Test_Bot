import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useSelector } from 'react-redux'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    }
}))

export default function Header() {
    const classes = useStyles()
    const charName = useSelector(state => state.user.currentAccount.name)
    const charStats = useSelector(state => state.user.stats)
    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    {charName}
                </Typography>
                <Grid container style={{ width: '100%' }}>
                    <Grid item xs={3}>
                        Expedition Points:
                        {charStats.expeditionPoints}/
                        {charStats.expeditionPointsMax}
                    </Grid>
                    <Grid item xs={3}>
                        Dungeon Points:
                        {charStats.dungeonPoints}/{charStats.dungeonPointsMax}
                    </Grid>
                    <Grid item xs={2}>
                        Gold: {charStats.gold}
                    </Grid>
                    <Grid item xs={2}>
                        Rubies: {charStats.rubies}
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}
