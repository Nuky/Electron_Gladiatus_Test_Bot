import React, {useEffect} from 'react';
import { DualRing } from "react-loading-io";
import {useDispatch} from "react-redux";
import {getAllCardGames} from "../../../actions/cardGame";

const Loading = (props) => {
    const dispatch = useDispatch();

    useEffect( ()=> {
        setTimeout(() => dispatch(getAllCardGames()), 1500);
    });

    return (
        <div>
           <DualRing size={props.size}/>
        </div>
    )
};

export default Loading;