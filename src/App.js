import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'wouter'
import { useSelector } from 'react-redux'

import RequireLogin from './utils/requireLogin'
import Login from './components/pages/login/index'
import Dashboard from './components/pages/dashboard/index'
import CustomizedSnackbar from './components/shared/util/Snackbar'
import Header from './components/shared/header/index'
import ExpeditionPage from './components/pages/expedition/index'

import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'

function App() {
    const shToken = useSelector(state => state.environment.shToken)

    let headerRender = () => {
        if (shToken.length > 0) {
            return <Header />
        }
    }
    return (
        <React.Fragment>
            <CssBaseline />
            <div className="App">
                <CustomizedSnackbar />
                {headerRender()}
                <Container>
                    <Route exact path="/" component={RequireLogin(Dashboard)} />
                    <Route exact path="/login" component={Login} />
                    <Route
                        exact
                        path="/expeditionPage"
                        component={ExpeditionPage}
                    />
                </Container>
            </div>
        </React.Fragment>
    )
}

export default App
