import React, { Component, useEffect } from 'react'
import { connect } from 'react-redux'
import { useLocation } from 'wouter'
import { useSelector } from 'react-redux'

const RequireLogin = CompostedComponent => {
    const [location, setLocation] = useLocation()
    const userLoggedIn = useSelector(state => state.user.userLoggedIn)
    console.log(userLoggedIn)
    useEffect(() => {
        if (!userLoggedIn) {
            console.log('User not logged in')
            setLocation('/login')
        }
    })
    return CompostedComponent
}

export default RequireLogin
