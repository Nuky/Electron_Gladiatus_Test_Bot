import axios from 'axios'

export function initAxios() {
    axios.defaults.withCredentials = true
}

export function setAuthorizationHeader(token) {
    if (token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    } else {
        delete axios.defaults.headers.common['Authorization']
    }
}
