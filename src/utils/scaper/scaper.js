import * as cheerio from 'cheerio'

export const getStats = htmlResponse => {
    const $ = cheerio.load(htmlResponse)
    let expeditionPoints = $('#expeditionpoints_value_point').text()
    let expeditionPointsMax = $('#expeditionpoints_value_pointmax').text()

    let dungeonPoints = $('#dungeonpoints_value_point').text()
    let dungeonPointsMax = $('#dungeonpoints_value_pointmax').text()

    let gold = $('#sstat_gold_val').text()
    let rubies = $('#sstat_ruby_val').text()

    let healthPointsPercent = $('#header_values_hp_percent').text().slice(0, -1)
    let experiencePercent = $('#header_values_xp_percent').text().slice(0, -1)
    let playerLevel = $('#header_values_level').text()

    return {
        expeditionPoints,
        expeditionPointsMax,
        dungeonPoints,
        dungeonPointsMax,
        gold,
        rubies,
        healthPointsPercent,
        experiencePercent,
        playerLevel
    }
}
