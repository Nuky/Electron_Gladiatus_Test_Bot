import { SET_EXPEDITION_DATA } from '../../constants/actionTypes'

export const setExpeditionData = payload => dispatch => {
    dispatch({ payload: { ...payload }, type: SET_EXPEDITION_DATA })
}
