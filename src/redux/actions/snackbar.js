import { SET_SNACKBAR } from '../../constants/actionTypes'

export const setSnackbar = (
  snackbarOpen,
  snackbarType,
  snackbarMessage
) => dispatch =>
  dispatch({
    type: SET_SNACKBAR,
    payload: {
      snackbarOpen,
      snackbarType,
      snackbarMessage
    }
  })
