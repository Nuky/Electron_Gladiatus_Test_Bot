import axios from 'axios'
import {
    LOGIN_USER,
    SET_ACCOUNTS,
    SET_ENV_AUTH_DATA,
    SET_ENV_LOGIN,
    SET_CURRENT_ACCOUNT,
    SET_SH_TOKEN,
    SET_PLAYER_STATS
} from '../../constants/actionTypes'
import { setAuthorizationHeader } from '../../utils/axiosHandler'
import { getStats } from '../../utils/scaper/scaper'
import { AUTH_URL, LOGIN_URL, USER_ACCOUNT_URL } from './../../constants/urls'

export const loginUser = payload => dispatch => {
    axios
        .post(LOGIN_URL, {
            autoGameAccountCreation:
                payload.environmentData.autoGameAccountCreation,
            gameEnvironmentId: payload.environmentData.gameEnvironmentId,
            gfLang: payload.environmentData.gfLang,
            identity: payload.email,
            locale: payload.environmentData.locale,
            password: payload.password,
            platformGameId: payload.environmentData.platformGameId
        })
        .then(response => {
            console.log(response)
            dispatch({
                type: SET_ENV_LOGIN,
                payload: response
            })
            dispatch({
                type: LOGIN_USER,
                payload: response
            })

            setAuthorizationHeader(response.data.token)
            dispatch(getAuthData())
        })
}

export const getAuthData = () => dispatch => {
    axios.get(AUTH_URL).then(response => {
        console.log(response)
        dispatch({ type: SET_ENV_AUTH_DATA, payload: response })
        dispatch(getAccountData())
    })
}

export const getAccountData = () => dispatch => {
    axios.get(USER_ACCOUNT_URL).then(response => {
        console.log(response)
        dispatch({ type: SET_ACCOUNTS, payload: response })
    })
}

export const setCurrentAccount = account => dispatch => {
    dispatch({ type: SET_CURRENT_ACCOUNT, payload: account })
    dispatch(
        loginAccount(account.id, account.server.number, account.server.language)
    )
}

export const loginAccount = (id, serverNumber, serverLanguage) => dispatch => {
    axios
        .get(
            `https://lobby.gladiatus.gameforge.com/api/users/me/loginLink?id=${id}&server[language]=${serverLanguage}&server[number]=${serverNumber}&clickedButton=account_list`
        )
        .then(response => dispatch(followLoginLink(response.data.url)))
}

export const followLoginLink = url => dispatch => {
    axios.get(url).then(response => {
        let shToken = response.request.responseURL.split('sh=')
        dispatch({
            type: SET_SH_TOKEN,
            payload: shToken[1]
        })
        dispatch(initMainScreen(response.request.responseURL))
    })

    //https://s37-de.gladiatus.gameforge.com/game/index.php?mod=settings&sh=73f330aca0d176ab9d5fe7ebf2e8bbcd
}

export const initMainScreen = url => dispatch => {
    axios.get(url).then(response => {
        let userStats = getStats(response.data)
        console.log(userStats)
        dispatch({ type: SET_PLAYER_STATS, payload: userStats })
    })
}
