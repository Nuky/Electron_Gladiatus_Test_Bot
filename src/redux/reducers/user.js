import {
    LOGIN_USER,
    SET_ACCOUNTS,
    SET_CURRENT_ACCOUNT,
    SET_PLAYER_STATS
} from './../../constants/actionTypes'
const DEFAULT_USER = {
    email: '',
    password: '',
    userLoggedIn: false,
    accounts: [],
    currentAccount: null,
    stats: {
        dungeonPoints: '',
        dungeonPointsMax: '',
        expeditionPoints: '',
        expeditionPointsMax: '',
        experiencePercent: '',
        gold: '',
        healthPointsPercent: '',
        playerLevel: '',
        rubies: ''
    }
}

function userReducer(state = DEFAULT_USER, action) {
    //console.log("UserReducer", action.payload);
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                userLoggedIn: true
            }
        case SET_ACCOUNTS:
            return {
                ...state,
                accounts: [...action.payload.data]
            }
        case SET_CURRENT_ACCOUNT:
            return { ...state, currentAccount: action.payload }
        case SET_PLAYER_STATS:
            return { ...state, stats: { ...action.payload } }
        default:
            return state
    }
}

export default userReducer
