import { combineReducers } from 'redux'
import userReducer from './user'
import snackbarReducer from './snackbar'
import environmentReducer from './environment'
import gameDataReducer from './gameData'
import botLogicReducer from './botLogic'

export default combineReducers({
    user: userReducer,
    snackbar: snackbarReducer,
    environment: environmentReducer,
    gameData: gameDataReducer,
    botLogic: botLogicReducer
})
