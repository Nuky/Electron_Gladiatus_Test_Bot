import { SET_EXPEDITION_DATA } from '../../constants/actionTypes'

const initialState = {
    expeditionBot: {
        attackingLocation: 0,
        attackingMonster: 0,
        running: false,
        delay: 25000,
        intervalId: ''
    }
}

function botLogicReducer(state = initialState, action) {
    switch (action.type) {
        case SET_EXPEDITION_DATA:
            return { ...state, expeditionBot: { ...action.payload } }
        default:
            return state
    }
}

export default botLogicReducer
