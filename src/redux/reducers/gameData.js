const initialState = {
    locations: [
        {
            name: 'Grimwood',
            monsters: ['Rat', 'Lynx', 'Wolf', 'Bear'],
            levelReq: 0
        },
        {
            name: 'Pirate Habour',
            monsters: ['RatPirate', 'LynxPirate', 'WolfPirate', 'BearPirate'],
            levelReq: 5
        },
        {
            name: 'Misty Mountains',
            monsters: ['RatMisty', 'LynxMisty', 'WolfMisty', 'BearMisty'],
            levelReq: 10
        },
        {
            name: 'Wolf Cave',
            monsters: ['RatWolf', 'LynxWolf', 'WolfWolf', 'BearWolf'],
            levelReq: 15
        }
    ]
}

function gameDataReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state
    }
}

export default gameDataReducer
