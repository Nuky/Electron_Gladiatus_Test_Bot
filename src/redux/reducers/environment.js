import { SET_ENV_LOGIN, SET_SH_TOKEN } from '../../constants/actionTypes'
import { SET_ENV_AUTH_DATA } from './../../constants/actionTypes'

const DEFAULT_ENVIRONMENT = {
    autoGameAccountCreation: 'false',
    gameEnvironmentId: '5c71b04b-a0c3-4792-be4f-81f97c25d8a8',
    gfLang: 'de',
    locale: 'de_DE',
    platformGameId: '07b3e4d5-f37e-440b-9aa5-b249121a6bfa',
    token: '',
    isPlatformLogin: '',
    isGameAccountMigrated: '',
    platformUserId: '',
    isGameAccountCreated: '',
    hasUnmigratedGameAccounts: '',
    id: 0,
    userId: 0,
    gameforgeAccountId: '',
    validated: false,
    portable: true,
    unlinkedAccounts: false,
    migrationRequired: false,
    email: '',
    unportableName: '',
    mhash: '',
    shToken: ''
}

function environmentReducer(state = DEFAULT_ENVIRONMENT, action) {
    //console.log("UserReducer", action.payload);
    console.log(action)
    switch (action.type) {
        case SET_ENV_LOGIN:
            return {
                ...state,
                ...action.payload.data
            }
        case SET_ENV_AUTH_DATA:
            return {
                ...state,
                ...action.payload.data
            }
        case SET_SH_TOKEN:
            return {
                ...state,
                shToken: action.payload
            }
        default:
            return state
    }
}

export default environmentReducer
